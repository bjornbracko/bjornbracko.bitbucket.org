
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";






var globalEHRID=""; 

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
    var sessionId=getSessionId();
    var ime;
    var priimek;
    var datumRojstva;
    var temperatura;
    var teza;
    var visina;
    var ehrId="";
    var sistolicniKrvniTlak;
    var diastolicniKrvniTlak;
    var pulz;

  var spol;
  var datumInUra=["2018-01-22T15:18:07.339+01:00","2018-02-22T15:18:07.339+01:00","2018-03-22T15:18:07.339+01:00","2018-04-22T15:18:07.339+01:00","2018-05-22T15:18:07.339+01:00"];
  
    switch(stPacienta) {
        case 1: 
            ime= "Janez";
            priimek="Umek";
            datumRojstva="1990-03-09T00:00:00";
            spol="MALE";
            visina=[170,170,171,171,171];
            teza=[70,73,72,74,74];
            temperatura=[36.7,37.0,36.5,38.0,36.8];
            sistolicniKrvniTlak=[110,115,113,120,125];
            diastolicniKrvniTlak=[70,65,75,80,70];
            pulz=[70,65,75,68,70];
            break;
        case 2: 
             ime= "Julija";
            priimek="Stare";
            spol="FEMALE";
            visina=[175,175,175,175,175];
            datumRojstva="1981-04-25T00:00:00";
            teza=[60,64,63,63,65];
            temperatura=[38.7,37.4,36.8,37.0,36.8];
            sistolicniKrvniTlak=[115,110,107,111,115];
            diastolicniKrvniTlak=[77,70,73,68,70];
            pulz=[80,76,77,70,81];
            break;
        case 3:
             ime= "Vid";
            priimek="Brzin";
            spol="MALE";
            visina=[180,180,180,180,179];
            teza=[95,91,87,80,81];
            temperatura=[36.5,36.8,36.7,37.1,37.5];
            datumRojstva="1975-01-01T00:00:00";
            sistolicniKrvniTlak=[130,125,129,135,131];
            diastolicniKrvniTlak=[85,79,80,86,80];
            pulz=[60,65,80,72,75];
            break;
    }
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
    $.ajax({
        url: baseUrl+"/ehr",
        type: 'POST',
        success: function(data) {
            ehrId=data.ehrId;
            switch(stPacienta) {
                case 1:  $("#1").val(ehrId);
                    break;
                case 2:  $("#2").val(ehrId);
                    break;
                case 3:   $("#3").val(ehrId);
                    break;
            }
            var partyData= {
                firstNames:ime,
                lastNames: priimek,
                dateOfBirth: datumRojstva,
                gender: spol,
                partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
            };
            $.ajax({
                url: baseUrl+"/demographics/party",
                type:'POST',
                contentType:'application/json',
                data: JSON.stringify(partyData),
                success:function(party) {
                    if(party.action=='CREATE') {
                        for(var i=0;i<5;i++) {
                            var podatki = {
                    		    "ctx/language": "en",
                    		    "ctx/territory": "SI",
                    		    "ctx/time": datumInUra[i],
                    		    "vital_signs/height_length/any_event/body_height_length": visina[i],
                    		    "vital_signs/body_weight/any_event/body_weight": teza[i],
                    		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak[i],
		                        "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak[i],
		                        "vital_signs/body_temperature/any_event/temperature|magnitude": temperatura[i],
		                        "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		                        "vital_signs/pulse/any_event/rate|magnitude": pulz[i],
                    		};
                            var parametriZahteve = {
                    		    ehrId: ehrId,
                    		    templateId: 'Vital Signs',
                    		    format: 'FLAT',
                    		    committer: 'Admin'
    		                };
                            $.ajax({
                                url:baseUrl+"/composition?"+$.param(parametriZahteve),
                                type:'POST',
                                contentType:'application/json',
                                data:JSON.stringify(podatki),
                                success: function(res) {
                                },
                                error: function(err) {
                                    console.log(err);
                                }
                            });
                        }    
                    }
                },
                error:function(err) {
                    console.log(err);
                }
            });
        },
        error: function(err) {
            console.log(err);
        }
        
    });
  // TODO: Potrebno implementirati

  return ehrId;
}

function generiraj() {
    for(var i=1;i<4;i++) {
        generirajPodatke(i);
        
    }
}

function ehrIdPrijava(){
        var sessionId=getSessionId();
        var ehrId=document.getElementById("preberiEHRid").value;
        globalEHRID=ehrId;
        if(ehrId.length !=36) {
            alert("Prosimo vnesite pravilni EhrId");
        } else {
            var ime;
            var priimek;
            var datumRojstva;
            var spol;
            $.ajax({
			    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	    type: 'GET',
	    	    headers: {"Ehr-Session": sessionId},
	    	    success: function (data) { 
	    	        var party=data.party;
	    	        ime=party.firstNames;
	    	        priimek=party.lastNames;
	    	        if(party.gender.indexOf('M')==0) {
	    	            spol="MOŠKI";
	    	        } else {
	    	            spol="ŽENSKI";
	    	        }
	    	        datumRojstva=party.dateOfBirth;
	    	        var temp=datumRojstva.split('-');
	    	        var starost=2018-temp[0];
	    	  
	    	        $('#ime').html(ime);
	    	        $('#priimek').html(priimek);
	    	        $('#ehrId').html(ehrId);
	    	        $('#spol').html(spol);
	    	        $('#datumrojstva').html(datumRojstva);
	    	        $('#starost').html(starost);
	    	        
	    
	    	        
	    	    },
	    	    error: function(err) {
	    	        alert("Nepoznan Ehr");
	    	        console.log(err);
	    	    }
            });
        }
    } 
var measurements=[];    
var time=[];
var naziv;
var query;


function getData(vitalniZnak, callback) {
    if(globalEHRID=="") {
        return;
    }
    switch(vitalniZnak) {
        case "temperatura":
            query="body_temperature";
            naziv="Temperatura [°C]";
            break;
        case "sTlak":
            naziv="Sistolični tlak [mm[Hg]]";
            query="blood_pressure";
            break;
            
        case "dTlak":
            naziv="Diastolični tlak [mm[Hg]]";
            query="blood_pressure";
            break;
        case "tTeža":
            naziv="Telesna Teža [kg]";
            query="weight";
            break;
        case "visina":
            naziv="Višina [cm]";
            query="height";
            break;
        case "pulz":
            naziv="Srčni utrip [/min]";
            query="pulse";
            break;
            
    }
    $.ajax({
       url: baseUrl+"/view/"+globalEHRID+"/"+query,
       type: "GET",
       headers: {"Ehr-Session": getSessionId()},
       success: function(data) {
           switch(vitalniZnak) {
               case "temperatura":
                   for(var i in data) {
                         measurements[i]=parseInt(data[i].temperature);
                         time[i]=data[i].time;
                         
                   }
                   break;
                case "tTeža":
                    for(var i in data) {
                        measurements[i]=parseInt(data[i].weight);
                        time[i]=data[i].time;
                   }
                   break;
                case "dTlak":
                    for(var i in data) {
                        measurements[i]=parseInt(data[i].diastolic);
                        time[i]=data[i].time;
                   }
                   break;
                case "sTlak":
                    for(var i in data) {
                        measurements[i]=parseInt(data[i].systolic);
                        time[i]=data[i].time;
                   }
                   break;
                case "visina":
                    for(var i in data) {
                        measurements[i]=parseInt(data[i].height);
                        time[i]=data[i].time;
                   }
                   break;
                case "pulz":
                    for(var i in data) {
                        measurements[i]=parseInt(data[i].pulse);
                        time[i]=data[i].time;
                   }
                   break;
                   
           }
       },
       error: function(err) {
         console.log(err);
       }
       
    });
    alert("Rišem graf");
    callback(vitalniZnak);
}
function drawChart(vitalniZnak) {
    var data;

    data = google.visualization.arrayToDataTable([
  ['Datum', naziv],
  ["Jan",  measurements[0]],
  ["Feb",  measurements[1]],
  ["Mar",  measurements[2]],
  ["Apr",  measurements[3]],
  ["Maj",  measurements[4]]
    ]);

    var options = {
      title: "",
      curveType: 'function',
      legend: { position: 'bottom' }
    };

    var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

    chart.draw(data, options);
}

window.addEventListener('load', function() {
    
    $('#prikaziVZ').change(function() {
        var temp=$(this).val();
        getData(temp, function(vitalniZnak) {
           drawChart(vitalniZnak); 
        });
	});
    
    $('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});
    
    



});



    
   


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
